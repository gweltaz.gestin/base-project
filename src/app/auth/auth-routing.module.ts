
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { VerifyEmailComponent } from './verify-email/verify-email.component';
import { RedirectComponent } from './redirect/redirect.component';
import { ResetComponent } from './reset/reset.component';
import { AngularFireAuthGuard, redirectLoggedInTo } from '@angular/fire/auth-guard';
import { Route } from '../shared/services/route';

const redirectUnauthorizedToDashboadIfLogin =
  () => redirectLoggedInTo(['dashboard']);

// Include route guard in routes array
const routes: Routes = [
  Route.withoutShell([
    { path: '', redirectTo: '/sign-in', pathMatch: 'full' },
    {
      path: 'sign-in', component: SignInComponent,
      canActivate: [AngularFireAuthGuard],
      data: { authGuardPipe: redirectUnauthorizedToDashboadIfLogin }
    },
    {
      path: 'register-user', component: SignUpComponent,
      canActivate: [AngularFireAuthGuard],
      data: { authGuardPipe: redirectUnauthorizedToDashboadIfLogin }
    },
    {
      path: 'forgot-password', component: ForgotPasswordComponent,
      canActivate: [AngularFireAuthGuard],
      data: { authGuardPipe: redirectUnauthorizedToDashboadIfLogin }
    },
    {
      path: 'verify-email-address', component: VerifyEmailComponent,
      canActivate: [AngularFireAuthGuard],
      data: { authGuardPipe: redirectUnauthorizedToDashboadIfLogin }
    },
    { path: 'redirect', component: RedirectComponent },
    { path: 'reset', component: ResetComponent }
  ])

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class AuthRoutingModule { }
