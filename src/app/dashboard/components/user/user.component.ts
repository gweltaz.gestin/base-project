import {Component, OnDestroy, OnInit} from '@angular/core';
import {User} from '../../../shared/interfaces/user';
import {Subscription} from 'rxjs';
import {NotifierService} from 'angular-notifier';
import {AuthService} from '../../../shared/services/auth.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit, OnDestroy {
  public user: User;
  sub: Subscription;
  notifier: NotifierService;
  constructor(
    public authService: AuthService,
    notifierService: NotifierService
  ) {
    this.notifier = notifierService;
  }

  ngOnInit() {
    this.sub = this.authService.user

      .subscribe((user) => {
        // console.log(user);
        this.user = user;

      });

  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
