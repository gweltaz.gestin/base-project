import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { ShareButtonsModule } from '@ngx-share/buttons';
import { SharedModule } from '../shared/shared.module';

import { DashboardComponent } from './containers/dashboard/dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
// import '../../icons';
import { faLinkedinIn } from '@fortawesome/free-brands-svg-icons/faLinkedinIn';
import { faTelegramPlane } from '@fortawesome/free-brands-svg-icons/faTelegramPlane';
import { faFacebookMessenger } from '@fortawesome/free-brands-svg-icons/faFacebookMessenger';
import { faWhatsapp } from '@fortawesome/free-brands-svg-icons/faWhatsapp';
import { faCommentAlt } from '@fortawesome/free-solid-svg-icons/faCommentAlt';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons/faEnvelope';
import { faLink } from '@fortawesome/free-solid-svg-icons/faLink';
import { UserComponent } from './components/user/user.component';

@NgModule({
  declarations: [
    DashboardComponent,
    UserComponent ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    ReactiveFormsModule,
    SharedModule,
    ShareButtonsModule,
    FontAwesomeModule
  ]
})
export class DashboardModule {
  constructor(library: FaIconLibrary) {
    // Add an icon to the library for convenient access in other components
    library.addIcons(faLinkedinIn, faTelegramPlane, faFacebookMessenger, faWhatsapp, faCommentAlt, faCommentAlt, faEnvelope, faLink);
  }
}
