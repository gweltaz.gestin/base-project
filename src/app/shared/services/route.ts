import {Route as ngRoute, Routes} from '@angular/router';
import {DashboardComponent} from 'src/app/dashboard/containers/dashboard/dashboard.component';
import {DashboardAdminComponent} from 'src/app/admin/containers/dashboard-admin/dashboard-admin.component';
import {AngularFireAuthGuard, redirectUnauthorizedTo} from '@angular/fire/auth-guard';
import {IsAdminGuard} from '../guard/is-admin.guard';

const redirectUnauthorizedToSignin = () => redirectUnauthorizedTo(['sign-in']);

/**
 * Provides helper methods to create routes.
 */
export class Route {

  /**
   * Creates routes using the shell component and authentication.
   * @param routes The routes to add.
   * @return  The new route using shell as the base.
   */
  static withShellAuth(routes: Routes): ngRoute {
    return {
      path: '',
      component: DashboardComponent,
      children: routes,
      canActivate: [AngularFireAuthGuard],
      // Reuse UserInterfaceComponent instance when navigating between child views
      data: {reuse: true, authGuardPipe: redirectUnauthorizedToSignin}
    };
  }

  static withShellAuthAdmin(routes: Routes): ngRoute {
    return {
      path: '',
      component: DashboardAdminComponent,
      children: routes,
      canActivate: [IsAdminGuard],
      // Reuse UserInterfaceComponent instance when navigating between child views
      data: {reuse: true}
    };
  }

  static withoutShell(routes: Routes): ngRoute {
    return {
      path: '',
      children: routes,
    };
  }
}
