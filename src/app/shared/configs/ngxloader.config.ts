import { NgxUiLoaderConfig } from 'ngx-ui-loader';

export  function ngxUiLoaderConfig(): NgxUiLoaderConfig  {
  return {
  'bgsColor': '#3F51B5',
  'bgsOpacity': 0.5,
  'bgsPosition': 'bottom-right',
  'bgsSize': 60,
  'bgsType': 'ball-spin-clockwise',
  'blur': 5,
  'delay': 0,
  'fgsColor': '#3F51B5',
  'fgsPosition': 'center-center',
  'fgsSize': 60,
  'fgsType': 'ball-spin-clockwise',
  'gap': 24,
  'logoPosition': 'center-center',
  'logoSize': 120,
  'logoUrl': '',
  'masterLoaderId': 'master',
  'overlayBorderRadius': '0',
  'overlayColor': 'rgba(246,246,246,0.8)',
  'pbColor': '#15CD72',
  'pbDirection': 'ltr',
  'pbThickness': 3,
  'hasProgressBar': true,
  'text': '',
  'textColor': '#FFFFFF',
  'textPosition': 'center-center',
  'maxTime': -1,
  'minTime': 500
};
}
