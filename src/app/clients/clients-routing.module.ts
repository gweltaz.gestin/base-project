import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListClientsComponent} from './containers/list-clients/list-clients.component';
import {Route} from '../shared/services/route';
import {AddClientComponent} from './containers/add-client/add-client.component';
import {EditClientComponent} from './containers/edit-client/edit-client.component';


const routes: Routes = [
  Route.withShellAuth([
    { path: 'list', component: ListClientsComponent},
    { path: 'edit/:id', component: EditClientComponent},
    { path: 'add', component: AddClientComponent},
  ])

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientsRoutingModule { }
