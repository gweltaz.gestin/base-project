import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ClientState} from '../../../shared/enums/client-state.enum';

@Component({
  selector: 'app-form-client',
  templateUrl: './form-client.component.html',
  styleUrls: ['./form-client.component.css']
})
export class FormClientComponent implements OnInit {
  myForm: FormGroup;
  @Input() initClient = {  name: '', email: '', state: 'Active'  };
  @Output() submitted = new EventEmitter();
  clientsStates = Object.values(ClientState);
  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.createForm();
  }

  createForm() {
     this.myForm = this.fb.group({
            name: [this.initClient.name, Validators.required],
            email: [this.initClient.email, Validators.email],
            state: this.initClient.state
       }
     );
  }

  register() {
    this.submitted.emit(this.myForm.value);
  }

}
