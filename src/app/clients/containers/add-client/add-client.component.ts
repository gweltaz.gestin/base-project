import { Component, OnInit } from '@angular/core';
import {Client} from '../../../shared/interfaces/client';
import {ClientService} from '../../services/client.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.css']
})
export class AddClientComponent implements OnInit {

  constructor(
    private clientService: ClientService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  addClient(data: Client) {
    console.log(data)
    this.clientService.create(data).subscribe(
      () => this.router.navigate(['/clients', 'list'])
    );
  }
}
