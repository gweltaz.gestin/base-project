import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import { Client } from 'src/app/shared/interfaces/client';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: HttpClient) { }



  list() {
      return this.http.get<Client[]>(`${environment.urlServer}clients`);
  }

  get(id: number) {
    return this.http.get<Client>(`${environment.urlServer}clients/${id}`);
  }

  update(data: Client) {
    return this.http.put<Client>(`${environment.urlServer}clients/${data.id}`, data);
  }

  delete(data: any) {
    return this.http.delete(`${environment.urlServer}clients/${data.id}`);
  }

  create(data: any) {
    return this.http.post<Client>(`${environment.urlServer}clients`, data);
  }
}
