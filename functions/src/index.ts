import * as functions from 'firebase-functions';

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
import * as admin from 'firebase-admin';
import * as _cors from 'cors';
import { functionsConfig } from './use/config/functions-config';


// CORS configuration.
const options: _cors.CorsOptions = {
    origin: functionsConfig.whitelist
};
const cors = _cors;

// Initializes Cloud Functions.
admin.initializeApp(functions.config().firebase);

// Firestore settings.
const db = admin.firestore();
db.settings({ timestampsInSnapshots: true });



// HTTP METHOD

/*export const checkSponsor= functions.https.onRequest((request: functions.Request, response: functions.Response) => {
  cors(options)(request, response, () => checkSponsorNumber(request, response));
});*/


/*




// TRIGGER PART

// Ajout d'un document provider
 export const createUser  = functions.firestore
.document('users/{userId}').onCreate(async(user, context)  => {

});





//SCHEDULE PART
// distribution des commissions
export const scheduledFunction = functions.pubsub.schedule('every 24 hours').onRun(() => {
  console.log('This will be run every 24 hours!');
  //return
});


export const createNextNetwork = functions.pubsub
.schedule('1 of month 00:00').onRun(() => {
  return createFutureNetwork()
});


// BACKUP


exports.scheduledFirestoreExport = functions.pubsub
                                            .schedule('every 24 hours')
                                            .onRun(async (context) => {
                                              await daily_bakcup()
});
*/
